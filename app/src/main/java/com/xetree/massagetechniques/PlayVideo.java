package com.xetree.massagetechniques;
import android.annotation.SuppressLint;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerView;


public class PlayVideo extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener{
 
 //public static final String API_KEY = "AIzaSyCM0IWaLdB4KZM9vpDn_DfjC_vqAH2_9zk";
 public static String video_id="";
 private AdView mAdView;
 private InterstitialAd interstitial;



    @SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.play_video);

        AdRequest adRequest = new AdRequest.Builder().addTestDevice("3E8310B58EAE303FF410A7B516BF17E7").build();
        // Prepare the Interstitial Ad
        interstitial = new InterstitialAd(PlayVideo.this);
        // Insert the Ad Unit ID
        interstitial.setAdUnitId("ca-app-pub-7639204738172604/8060761973");
        // Load ads into Interstitial Ads
        interstitial.loadAd(adRequest);

        // Prepare an Interstitial Ad Listener
        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {

            }
        });


        if(!haveNetworkConnection())
        {
            android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this)
                    .setMessage("There is some problem with your internet connect please fix and retry...")
                    .setTitle("Internet Problem!")
                    .setNeutralButton("Ok, I got it!", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            finish();
                        }
                    });

            android.app.AlertDialog dialog = builder.create();
            dialog.show();
        }
        else {

            final String youtube_uri="com.google.android.youtube";
            if(!appInstalledOrNot(youtube_uri))
            {
                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(this)
                        .setMessage("There is some problem with your internet connect please fix and retry...")
                        .setTitle("Internet Problem!")
                        .setNeutralButton("Install", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                try {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + youtube_uri)));
                                } catch (android.content.ActivityNotFoundException anfe) {
                                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + youtube_uri)));
                                }

                                finish();

                            }
                        });

                android.app.AlertDialog dialog = builder.create();
                dialog.show();
            }
            else {

                Intent in = getIntent();

                video_id = in.getStringExtra("video_id");

                YouTubePlayerView youTubePlayerView = (YouTubePlayerView) findViewById(R.id.youtubeplayerview);
                youTubePlayerView.initialize(ApiKey.YOUTUBE_API_KEY, this);
            }
        }
    }

    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
    
	public void displayInterstitial() {
		// If Ads are loaded, show Interstitial else show nothing.
		if (interstitial.isLoaded()) {
			interstitial.show();
		}
	}

    private boolean appInstalledOrNot(String uri) {

        PackageManager pm = getPackageManager();
        boolean app_installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed;
    }
	

 @Override
 public void onInitializationFailure(Provider provider,
   YouTubeInitializationResult result) {
  Toast.makeText(getApplicationContext(), 
    "onInitializationFailure()", 
    Toast.LENGTH_LONG).show();
 }

 @Override
 public void onInitializationSuccess(Provider provider, YouTubePlayer player,
   boolean wasRestored) {
  if (!wasRestored) {
        player.cueVideo(video_id);
      }
 }
 

 @Override
 public void onBackPressed()
 {
     super.onBackPressed();
     // Do your things.
     displayInterstitial();
 }

}