package com.xetree.massagetechniques;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.ashokvarma.bottomnavigation.BottomNavigationBar;
import com.ashokvarma.bottomnavigation.BottomNavigationItem;
import com.xetree.massagetechniques.model.VideoCategory;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String YOUTUBE_APP_KEY = "youtube_auth_key";
    private static final String CATEGORY_ID = "cat_id";
    private static final String CATEGORY_TITLE = "vcat_title";
    private static final String CATEGORY_IMAGE_URL = "vcat_img_url";
    private static final String CATEGORY_PLAYLIST_ID = "vcat_playlist_id";


    private RecyclerView recyclerView;
    private CategoriesAdapter adapter;
    private List<VideoCategory> categoryList;
    private AdView mAdView;

    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        /*
        BottomNavigationBar bottomNavigationBar = (BottomNavigationBar) findViewById(R.id.bottom_navigation_bar);

        bottomNavigationBar
                .addItem(new BottomNavigationItem(R.drawable.ic_thumb_up, "Home"))
                .addItem(new BottomNavigationItem(R.drawable.ic_thumb_down, "Books"))

                .initialise();

        bottomNavigationBar.setTabSelectedListener(new BottomNavigationBar.OnTabSelectedListener(){
            @Override
            public void onTabSelected(int position) {
            }
            @Override
            public void onTabUnselected(int position) {
            }
            @Override
            public void onTabReselected(int position) {
            }
        });
        */
        /*

        MobileAds.initialize(getApplicationContext(), "ca-app-pub-7639204738172604/1903876375");
        AdView mAdView = (AdView) findViewById(R.id.adView);
        mAdView.bringToFront();
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        */


        boolean isFirstRun = getSharedPreferences("PREFERENCE", MODE_PRIVATE).getBoolean("isFirstRun", true);
        if (isFirstRun){

            // 1. Instantiate an AlertDialog.Builder with its constructor
            AlertDialog.Builder builder = new AlertDialog.Builder(this);

            // 2. Chain together various setter methods to set the dialog characteristics
            builder.setMessage(R.string.dialog_message)
                    .setTitle(R.string.dialog_title);

            // Add the buttons
            builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User clicked OK button
                    getSharedPreferences("PREFERENCE", MODE_PRIVATE)
                            .edit()
                            .putBoolean("isFirstRun", false)
                            .apply();
                }
            });
            builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    // User cancelled the dialog
                    finish();
                }
            });

            // 3. Get the AlertDialog from create()
            AlertDialog dialog = builder.create();
            dialog.show();
        }


        progressBar=(ProgressBar)findViewById(R.id.progressBar3);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);

        categoryList = new ArrayList<>();
        adapter = new CategoriesAdapter(this, categoryList);

        RecyclerView.LayoutManager mLayoutManager;
        Resources resources = getResources();
        if (resources.getBoolean(R.bool.isTablet)) {
            mLayoutManager = new GridLayoutManager(this, 3);
        }
        else
        {
            mLayoutManager = new GridLayoutManager(this, 2);
        }

        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.addItemDecoration(new GridSpacingItemDecoration(2, dpToPx(10), true));
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);

        prepareVideoCategorys();

        try {
            //Glide.with(this).load(R.drawable.cover_image).into((ImageView) findViewById(R.id.backdrop));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Adding few albums for testing
     */
    private void prepareVideoCategorys() {

        VideoCategory videoCategory=new VideoCategory();

        videoCategory.setId("010");
        videoCategory.setTitle("Saved Videos");
        videoCategory.setThumbnail(R.drawable.saved_videos);
        videoCategory.setPlayListId("saved");

        categoryList.add(videoCategory);

        /*
        int[] covers = new int[]{
                R.drawable.album1,
                R.drawable.album2,
                R.drawable.album3,
                R.drawable.album4,
                R.drawable.album5,
                R.drawable.album6,
                R.drawable.album7,
                R.drawable.album8,
                R.drawable.album9,
                R.drawable.album10,
                R.drawable.album11};

        VideoCategory a = new VideoCategory("True Romance", 13, covers[0]);
        categoryList.add(a);

        a = new VideoCategory("Xscpae", 8, covers[1]);
        categoryList.add(a);

        a = new VideoCategory("Maroon 5", 11, covers[2]);
        categoryList.add(a);

        a = new VideoCategory("Born to Die", 12, covers[3]);
        categoryList.add(a);

        a = new VideoCategory("Honeymoon", 14, covers[4]);
        categoryList.add(a);

        a = new VideoCategory("I Need a Doctor", 1, covers[5]);
        categoryList.add(a);

        a = new VideoCategory("Loud", 11, covers[6]);
        categoryList.add(a);

        a = new VideoCategory("Legend", 14, covers[7]);
        categoryList.add(a);

        a = new VideoCategory("Hello", 11, covers[8]);
        categoryList.add(a);

        a = new VideoCategory("Greatest Hits", 17, covers[9]);
        categoryList.add(a);
        */
        makeJsonArrayRequest();

    }

    /**
     * RecyclerView item decoration - give equal margin around grid item
     */
    public class GridSpacingItemDecoration extends RecyclerView.ItemDecoration {

        private int spanCount;
        private int spacing;
        private boolean includeEdge;

        public GridSpacingItemDecoration(int spanCount, int spacing, boolean includeEdge) {
            this.spanCount = spanCount;
            this.spacing = spacing;
            this.includeEdge = includeEdge;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            int position = parent.getChildAdapterPosition(view); // item position
            int column = position % spanCount; // item column

            if (includeEdge) {
                outRect.left = spacing - column * spacing / spanCount; // spacing - column * ((1f / spanCount) * spacing)
                outRect.right = (column + 1) * spacing / spanCount; // (column + 1) * ((1f / spanCount) * spacing)

                if (position < spanCount) { // top edge
                    outRect.top = spacing;
                }
                outRect.bottom = spacing; // item bottom
            } else {
                outRect.left = column * spacing / spanCount; // column * ((1f / spanCount) * spacing)
                outRect.right = spacing - (column + 1) * spacing / spanCount; // spacing - (column + 1) * ((1f /    spanCount) * spacing)
                if (position >= spanCount) {
                    outRect.top = spacing; // item top
                }
            }
        }
    }

    /**
     * Converting dp to pixel
     */
    private int dpToPx(int dp) {
        Resources r = getResources();
        return Math.round(TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, r.getDisplayMetrics()));
    }

    /**
     * Method to make json array request where response starts with [
     * */
    private void makeJsonArrayRequest() {

        showProgress();

        JsonArrayRequest req = new JsonArrayRequest(AppController.API_PATH,
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {


                        //Log.v("NotifyChanged",response.toString());

                        try {

                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = (JSONObject) response.get(i);
                                //Log.v("NotifyChanged",jsonObject.toString());
                                if(i==0)
                                {
                                    if (jsonObject.has(YOUTUBE_APP_KEY) && !jsonObject.isNull(YOUTUBE_APP_KEY)) {
                                        ApiKey.YOUTUBE_API_KEY=jsonObject.getString(YOUTUBE_APP_KEY);
                                    }
                                }
                                else
                                {
                                    VideoCategory videoCategory=new VideoCategory();
                                    if (jsonObject.has(CATEGORY_ID) && !jsonObject.isNull(CATEGORY_ID)) {
                                        videoCategory.setId(jsonObject.getString(CATEGORY_ID));
                                    }
                                    if (jsonObject.has(CATEGORY_TITLE) && !jsonObject.isNull(CATEGORY_TITLE)) {
                                        videoCategory.setTitle(jsonObject.getString(CATEGORY_TITLE));
                                    }
                                    if (jsonObject.has(CATEGORY_IMAGE_URL) && !jsonObject.isNull(CATEGORY_IMAGE_URL)) {
                                        videoCategory.setImageUrl(jsonObject.getString(CATEGORY_IMAGE_URL));
                                    }
                                    if (jsonObject.has(CATEGORY_PLAYLIST_ID) && !jsonObject.isNull(CATEGORY_PLAYLIST_ID)) {
                                        videoCategory.setPlayListId(jsonObject.getString(CATEGORY_PLAYLIST_ID));
                                    }
                                    categoryList.add(videoCategory);
                                    Log.v("NotifyChanged",videoCategory.getTitle());
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getApplicationContext(),
                                    "Error: " + e.getMessage(),
                                    Toast.LENGTH_LONG).show();

                            Log.v("NotifyChangedException",e.toString());
                        }

                        adapter.notifyDataSetChanged();
                        hidePorgress();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(MainActivity.this)
                        .setMessage("There is some problem with your internet connect please fix and retry...")
                        .setTitle("Internet Problem!")
                        .setNeutralButton("Ok, I got it!", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //.....
                            }
                        });

                android.app.AlertDialog dialog = builder.create();
                dialog.show();

                hidePorgress();
            }
        });
        // Adding request to request queue
        AppController.getInstance().addToRequestQueue(req);
    }

    public void showProgress()
    {
        progressBar.setVisibility(View.VISIBLE);

    }
    public void hidePorgress()
    {
        progressBar.setVisibility(View.GONE);
    }
}