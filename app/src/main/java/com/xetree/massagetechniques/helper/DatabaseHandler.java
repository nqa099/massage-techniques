package com.xetree.massagetechniques.helper;

/**
 * Created by Farhan on 9/23/2016.
 */
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.xetree.massagetechniques.model.MyVideo;

public class DatabaseHandler extends SQLiteOpenHelper {

    // All Static variables
    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "videosManager";

    // Contacts table name
    private static final String TABLE_VIDEOS = "videos";

    // Contacts Table Columns names
    private static final String KEY_ID = "id";
    private static final String  ID= "vide_id";
    private static final String TITLE = "title";
    private static final String VIEW_COUNT = "view_count";
    private static final String DURATION = "duration";
    private static final String THUMBNAIL_URL = "thumbnail_url";


    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_VIDEOS + "("
                + KEY_ID + " INTEGER PRIMARY KEY," + ID + " TEXT,"
                + TITLE + " TEXT,"
                + VIEW_COUNT + " TEXT,"
                + DURATION + " TEXT,"
                + THUMBNAIL_URL + " TEXT" + ")";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VIDEOS);
        // Create tables again
        onCreate(db);
    }

    /**
     * All CRUD(Create, Read, Update, Delete) Operations
     */

    // Adding new contact
    public void saveVideo(MyVideo myVideo) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(ID, myVideo.getId());
        values.put(TITLE, myVideo.getTitle());
        values.put(VIEW_COUNT, myVideo.getViewCount());
        values.put(DURATION, myVideo.getDuration());
        values.put(THUMBNAIL_URL, myVideo.getThumbnailUrl());

        // Inserting Row
        db.insert(TABLE_VIDEOS, null, values);
        db.close(); // Closing database connection
    }

    /*
    // Getting single contact
    Contact getContact(int id) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_CONTACTS, new String[] { KEY_ID,
                        KEY_NAME, KEY_PH_NO }, KEY_ID + "=?",
                new String[] { String.valueOf(id) }, null, null, null, null);
        if (cursor != null)
            cursor.moveToFirst();

        Contact contact = new Contact(Integer.parseInt(cursor.getString(0)),
                cursor.getString(1), cursor.getString(2));
        // return contact
        return contact;
    }
    */

    // Getting All Contacts
    public List<MyVideo> getAllContacts() {
        List<MyVideo> videostList = new ArrayList<MyVideo>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_VIDEOS;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                MyVideo myVideo = new MyVideo();
                myVideo.setKey_id(Integer.parseInt(cursor.getString(0)));
                myVideo.setId(cursor.getString(1));
                myVideo.setTitle(cursor.getString(2));
                myVideo.setViewCount(cursor.getString(3));
                myVideo.setDuration(cursor.getString(4));
                myVideo.setThumbnailUrl(cursor.getString(5));
                // Adding video to list
                videostList.add(myVideo);
            } while (cursor.moveToNext());
        }

        // return contact list
        return videostList;
    }

    /*
    // Updating single contact
    public int updateContact(Contact contact) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, contact.getName());
        values.put(KEY_PH_NO, contact.getPhoneNumber());

        // updating row
        return db.update(TABLE_CONTACTS, values, KEY_ID + " = ?",
                new String[] { String.valueOf(contact.getID()) });
    }
    */
    // Deleting single contact
    public void deleteContact(MyVideo myVideo) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_VIDEOS, KEY_ID + " = ?",
                new String[] { String.valueOf(myVideo.getKey_id()) });
        db.close();
    }


    // Getting contacts Count
    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + TABLE_VIDEOS;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        int c=cursor.getCount();
        cursor.close();

        // return count
        return c;
    }

}